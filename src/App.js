import React, { useState } from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";

function App() {
  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);

  const closeModals = () => {
    setIsFirstModalOpen(false);
    setIsSecondModalOpen(false);
  };

  const openFirstModal = () => {
    setIsFirstModalOpen(true);
    setIsSecondModalOpen(false);
  };

  const openSecondModal = () => {
    setIsSecondModalOpen(true);
    setIsFirstModalOpen(false);
  };

  return (
    <div className="App">
      <Button
        backgroundColor="blue"
        text="Open first modal"
        onClick={openFirstModal}
      />
      <Button
        backgroundColor="green"
        text="Open second modal"
        onClick={openSecondModal}
      />

      {isFirstModalOpen && (
        <Modal
          header="Do you want to delete this file?"
          closeButton={true}
          text={
            <>
              Once you delete this file, it won't be possible to undo this
              action.
              <br />
              Are you sure you want to delete it?
            </>
          }
          actions={
            <>
              <Button
                backgroundColor="#ac2828"
                text="Ok"
                onClick={() => setIsFirstModalOpen(false)}
              />
              <Button
                backgroundColor="#ac2828"
                text="Cancel"
                onClick={() => setIsFirstModalOpen(false)}
              />
            </>
          }
          onClose={closeModals}
        />
      )}

      {isSecondModalOpen && (
        <Modal
          header={<>ERROR</>}
          closeButton={true}
          text={
            <>
              Your data won't be saved!
              <br />
              Are you sure you want to quit registration process?
            </>
          }
          actions={
            <>
              <Button
                backgroundColor="#ac2828"
                text="Cancel"
                onClick={() => setIsSecondModalOpen(false)}
              />
              <Button
                backgroundColor="#ac2828"
                text="Yes"
                onClick={() => setIsSecondModalOpen(false)}
              />
            </>
          }
          onClose={closeModals}
        />
      )}
    </div>
  );
}

export default App;
