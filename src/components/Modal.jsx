import React from "react";
import styles from "./Modal.module.scss";

const Modal = ({ header, closeButton, text, actions, onClose }) => {
  return (
    <div className={styles["modal-overlay"]} onClick={onClose}>
      <div className={styles.modal}>
        <div className={styles["modal-header-container"]}>
          <h2 className={styles["modal-header"]}>{header}</h2>
          {closeButton && (
            <span className={styles["close-button"]} onClick={onClose}>
              &times;
            </span>
          )}
        </div>
        <div className={styles["modal-content"]}>{text}</div>
        <div className={styles["modal-actions"]}>{actions}</div>
      </div>
    </div>
  );
};

export default Modal;
